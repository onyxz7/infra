# Personal Infrastructure as Code

This repository contains Ansible playbooks and roles for deploying a complete home server infrastructure with various services. It automates the setup of storage, system configuration, and deployment of containerized services.

## Features

### System Configuration
- Custom SSH port configuration with fallback mechanisms
- User management and security hardening
- Docker installation and configuration
- RAID storage setup
- System packages and services management

### Core Services
- Traefik (Reverse Proxy)
- Portainer (Container Management)
- Pi-hole (DNS)
- Cloudflare DDNS
- File Browser
- Personal Website
- Linktree

### Media Services
- Jellyfin (Media Server)
- Radarr (Movies)
- Sonarr (TV Shows)
- Bazarr (Subtitles)
- Jellyseerr (Media Requests)
- Prowlarr (Indexer)

### Download Services
- qBittorrent (with VPN)

## Prerequisites

- Ansible 2.9+
- Python 3.x
- Target machine running Ubuntu Server
- (Optional) 1Password CLI for vault password management

## Quick Start

1. Clone the repository:
```bash
git clone https://gitlab.com/npetelle/infra.git
```

2. Set up secrets using Ansible Vault:
```bash
# Create an encrypted secrets file from the template
cp group_vars/all/secret.default.yml group_vars/all/secret.yml
ansible-vault encrypt group_vars/all/secret.yml

# Edit the encrypted secrets file with your actual values
ansible-vault edit group_vars/all/secret.yml

# If using 1Password, create a vault-id file with your vault password item UUID
echo "your-1password-vault-id" > vault-id

3. Modify variables in `group_vars/all/vars.yml`:
- Set your username
- Configure domain
- Adjust storage settings
- Customize service options

4. Update inventory in `hosts` file with your server IP

5. Run the playbook:
```bash
./run.sh
```

## Configuration

### Important Files
- `group_vars/all/vars.yml`: Main configuration file
- `group_vars/all/secret.yml`: Sensitive data (passwords, API keys)
- `hosts`: Inventory file
- `ansible.cfg`: Ansible configuration - no need to change

### Storage Configuration
- RAID setup using mdadm
- Configurable disk paths and RAID levels
- Automated filesystem creation and mounting

### Network Configuration
- Custom SSH port with fallback mechanism
- Traefik reverse proxy setup
- Cloudflare DNS integration

## Security

- SSH password authentication disabled
- Custom SSH port
- Automatic system updates
- Docker network isolation
- HTTPS with Let's Encrypt

## Acknowledgments

- Wolfgang (notthebee) for [Infra](https://github.com/notthebee/infra), which inspired this project
- Larry Smith Jr. for [ansible-mdadm](https://github.com/mrlesmithjr/ansible-mdadm)

## Warning
This project is for educational purpose only. I don't take any responsibility for any damage caused by this project. Use at your own risk.
