#!/bin/bash

# Function to check if op is available and signed in
check_op() {
    if ! command -v op &> /dev/null; then
        return 1
    fi

    if ! op account get &> /dev/null; then
        return 1
    fi

    return 0
}

# Get vault ID from file
VAULT_ID_FILE="$(dirname "$0")/vault-id"
if [ ! -f "$VAULT_ID_FILE" ]; then
    exit 1
fi
VAULT_ID=$(cat "$VAULT_ID_FILE")

# Try to get password from 1Password
if check_op; then
    op item get "$VAULT_ID" --field password --reveal 2>/dev/null
    exit $?
fi

exit 1
