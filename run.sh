#!/bin/bash

if [ -x "./vault-pass.sh" ]; then
    PASSWORD=$(./vault-pass.sh 2>/dev/null)
    if [ $? -eq 0 ] && [ ! -z "$PASSWORD" ]; then
        echo "$PASSWORD" | ansible-playbook --vault-password-file /dev/stdin run.yml
        exit $?
    fi
fi

# Fallback to standard ansible-playbook with password prompt
ansible-playbook --ask-vault-pass run.yml
